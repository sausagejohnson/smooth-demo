/**
 * @file smooth-demo.cpp
 * @date 17/05/2017
 * @author sausage@zeta.org.au
 *
 * Orx Game template
 */


#include "orx.h"

orxOBJECT *sceneObject;
orxOBJECT *cameraHandle;
orxCAMERA *camera;
int tileLength = 64;
int tilesAcross = 50;
int tilesDown = 50;
const int MAX_BALLS = 20;
int numberOfBallsCreated = 0;
const int MAX_SKIPPED_CYCLES = 2;
int skippedCycles = 0;
orxCLOCK *createBallsClock;
/*
 * This is a basic C++ template to quickly and easily get started with a project or tutorial.
 */


const orxSTRING GetRandomTileName(){
	orxConfig_PushSection("Tiles");
	const orxSTRING tileName = orxConfig_GetListString("TileList", -1);
	orxConfig_PopSection();
	
	return tileName;
}

void orxFASTCALL Update(const orxCLOCK_INFO *_pstClockInfo, void *_pContext)
{
	orxVECTOR cameraPosition = { 0,0,0 };
	orxCamera_GetPosition(camera, &cameraPosition);
 
	orxVECTOR cameraHandlePosition= { 0,0,0 };
	orxObject_GetPosition(cameraHandle, &cameraHandlePosition);
 
	cameraPosition.fX = cameraHandlePosition.fX;
	cameraPosition.fY = cameraHandlePosition.fY;
	orxCamera_SetPosition(camera, &cameraPosition);
}

void orxFASTCALL CreateBalls(const orxCLOCK_INFO *_pstClockInfo, void *_pstContext){
	if (skippedCycles < MAX_SKIPPED_CYCLES){
		skippedCycles++;
		return;
	}
	
   orxObject_CreateFromConfig("Ball");
   numberOfBallsCreated++;
   if (numberOfBallsCreated >= MAX_BALLS){
	   orxClock_Pause(createBallsClock);
   }
}

/** Initializes your game
 */
orxSTATUS orxFASTCALL Init()
{
    /* Displays a small hint in console */
    orxLOG("\n* This template creates a viewport/camera couple and an object"
    "\n* You can play with the config parameters in ../data/config/smooth-demo.ini"
    "\n* After changing them, relaunch the template to see the changes.");

    /* Creates the viewport */
    orxVIEWPORT *viewport = orxViewport_CreateFromConfig("Viewport");
	camera = orxViewport_GetCamera(viewport);

    /* Creates the object */
    sceneObject = orxObject_CreateFromConfig("Scene");
	cameraHandle = orxObject_CreateFromConfig("CameraHandle");
	
	int left = - ((tilesAcross * tileLength)/2);
	int top = - ((tilesDown * tileLength)/2);

	for (int y = 0; y < tilesDown; y++){
		for (int x = 0; x < tilesAcross; x++){
			orxOBJECT *obj = orxObject_CreateFromConfig(GetRandomTileName());
			orxVECTOR pos = { (x*tileLength) + left, (y*tileLength) + top, 0 };
			orxObject_SetPosition(obj, &pos);
		}
	}

	orxClock_Register(orxClock_FindFirst(orx2F(-1.0f), orxCLOCK_TYPE_CORE), Update, orxNULL, orxMODULE_ID_MAIN, orxCLOCK_PRIORITY_NORMAL);

	createBallsClock = orxClock_Create(orx2F(0.2f), orxCLOCK_TYPE_USER); 
    orxClock_Register(createBallsClock, CreateBalls, orxNULL, orxMODULE_ID_MAIN, orxCLOCK_PRIORITY_NORMAL);

    /* Done! */
    return orxSTATUS_SUCCESS;
}

/** Run function, is called every clock cycle
 */
orxSTATUS orxFASTCALL Run()
{
    orxSTATUS eResult = orxSTATUS_SUCCESS;

    /* Should quit? */
    if(orxInput_IsActive("Quit"))
    {
        /* Updates result */
        eResult = orxSTATUS_FAILURE;
    }
	  
	  if(orxInput_HasBeenActivated("ToggleProfiler"))
	  {
		/* Toggles profiler rendering */
		orxConfig_PushSection(orxRENDER_KZ_CONFIG_SECTION);
		orxConfig_SetBool(orxRENDER_KZ_CONFIG_SHOW_PROFILER, !orxConfig_GetBool(orxRENDER_KZ_CONFIG_SHOW_PROFILER));
		orxConfig_PopSection();
	  }

    /* Done! */
    return eResult;
}

/** Exit function
 */
void orxFASTCALL Exit()
{
    /* Lets Orx clean all our mess automatically. :) */
}

/** Bootstrap function
 */
orxSTATUS orxFASTCALL Bootstrap()
{
    orxSTATUS eResult = orxSTATUS_SUCCESS;

    /* Adds a config storage to find the initial config file */
    orxResource_AddStorage(orxCONFIG_KZ_RESOURCE_GROUP, "../data/config", orxFALSE);

    /* Done! */
    return eResult;
}

/** Main function
 */
int main(int argc, char **argv)
{
    /* Sets bootstrap function to provide at least one resource storage before loading any config files */
    orxConfig_SetBootstrap(Bootstrap);

    /* Executes a new instance of tutorial */
    orx_Execute(argc, argv, Init, Run, Exit);

    /* Done! */
    return EXIT_SUCCESS;
}
