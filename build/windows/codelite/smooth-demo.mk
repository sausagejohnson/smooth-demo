##
## Auto Generated makefile by CodeLite IDE
## any manual changes will be erased      
##
## Debug
ProjectName            :=smooth-demo
ConfigurationName      :=Debug
WorkspacePath          := "C:\Work\Dev\orx-projects\smooth-demo\build\windows\codelite"
ProjectPath            := "C:\Work\Dev\orx-projects\smooth-demo\build\windows\codelite"
IntermediateDirectory  :=$(ConfigurationName)
OutDir                 := $(IntermediateDirectory)
CurrentFileName        :=
CurrentFilePath        :=
CurrentFileFullPath    :=
User                   :=sausage
Date                   :=10/01/2019
CodeLitePath           :="C:\Program Files\CodeLite"
LinkerName             :=C:/MinGW-6.3.0/bin/g++.exe
SharedObjectLinkerName :=C:/MinGW-6.3.0/bin/g++.exe -shared -fPIC
ObjectSuffix           :=.o
DependSuffix           :=.o.d
PreprocessSuffix       :=.i
DebugSwitch            :=-g 
IncludeSwitch          :=-I
LibrarySwitch          :=-l
OutputSwitch           :=-o 
LibraryPathSwitch      :=-L
PreprocessorSwitch     :=-D
SourceSwitch           :=-c 
OutputFile             :=../../../bin/smooth-demod.exe
Preprocessors          :=$(PreprocessorSwitch)__orxDEBUG__ 
ObjectSwitch           :=-o 
ArchiveOutputSwitch    := 
PreprocessOnlySwitch   :=-E
ObjectsFileList        :="smooth-demo.txt"
PCHCompileFlags        :=
MakeDirCommand         :=makedir
RcCmpOptions           := 
RcCompilerName         :=C:/MinGW-6.3.0/bin/windres.exe
LinkOptions            :=  -static-libgcc -static-libstdc++
IncludePath            :=  $(IncludeSwitch). $(IncludeSwitch)$(ORX)/include 
IncludePCH             := 
RcIncludePath          := 
Libs                   := $(LibrarySwitch)orxd 
ArLibs                 :=  "orxd" 
LibPath                := $(LibraryPathSwitch). $(LibraryPathSwitch)$(ORX)/lib/dynamic $(LibraryPathSwitch). 

##
## Common variables
## AR, CXX, CC, AS, CXXFLAGS and CFLAGS can be overriden using an environment variables
##
AR       := C:/MinGW-6.3.0/bin/ar.exe rcu
CXX      := C:/MinGW-6.3.0/bin/g++.exe
CC       := C:/MinGW-6.3.0/bin/gcc.exe
CXXFLAGS :=  -ffast-math -g -msse2 -fno-exceptions $(Preprocessors)
CFLAGS   :=  -ffast-math -g -msse2 -fno-exceptions $(Preprocessors)
ASFLAGS  := 
AS       := C:/MinGW-6.3.0/bin/as.exe


##
## User defined environment variables
##
CodeLiteDir:=C:\Program Files (x86)\CodeLite
Objects0=$(IntermediateDirectory)/src_smooth-demo.cpp$(ObjectSuffix) 



Objects=$(Objects0) 

##
## Main Build Targets 
##
.PHONY: all clean PreBuild PrePreBuild PostBuild MakeIntermediateDirs
all: $(OutputFile)

$(OutputFile): $(IntermediateDirectory)/.d $(Objects) 
	@$(MakeDirCommand) $(@D)
	@echo "" > $(IntermediateDirectory)/.d
	@echo $(Objects0)  > $(ObjectsFileList)
	$(LinkerName) $(OutputSwitch)$(OutputFile) @$(ObjectsFileList) $(LibPath) $(Libs) $(LinkOptions)

PostBuild:
	@echo Executing Post Build commands ...
	cmd /c copy /Y $(ORX)\lib\dynamic\orx*.dll ..\..\..\bin
	@echo Done

MakeIntermediateDirs:
	@$(MakeDirCommand) "$(ConfigurationName)"


$(IntermediateDirectory)/.d:
	@$(MakeDirCommand) "$(ConfigurationName)"

PreBuild:


##
## Objects
##
$(IntermediateDirectory)/src_smooth-demo.cpp$(ObjectSuffix): ../../../src/smooth-demo.cpp $(IntermediateDirectory)/src_smooth-demo.cpp$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "C:/Work/Dev/orx-projects/smooth-demo/src/smooth-demo.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/src_smooth-demo.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/src_smooth-demo.cpp$(DependSuffix): ../../../src/smooth-demo.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/src_smooth-demo.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/src_smooth-demo.cpp$(DependSuffix) -MM "../../../src/smooth-demo.cpp"

$(IntermediateDirectory)/src_smooth-demo.cpp$(PreprocessSuffix): ../../../src/smooth-demo.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/src_smooth-demo.cpp$(PreprocessSuffix) "../../../src/smooth-demo.cpp"


-include $(IntermediateDirectory)/*$(DependSuffix)
##
## Clean
##
clean:
	$(RM) -r $(ConfigurationName)/


